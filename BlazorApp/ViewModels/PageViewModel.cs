﻿using System;

namespace BlazorApp.ViewModels
{
    public class PageViewModel
    {
        public int PageNumber = 1;
        public int TotalPages { get; private set; }
        public static PageViewModel list = new PageViewModel();
        public PageViewModel(int count, int pageNumber, int pageSize)
        {
            int total = (int)Math.Ceiling(count / (double)pageSize);
            PageNumber = pageNumber;
            TotalPages = total;
            list.TotalPages = total;
        }
        public PageViewModel()
        {

        }
        public bool HasPreviousPage
        {
            get
            {
                return (list.PageNumber > 1);
            }
        }

        public bool HasNextPage
        {
            get
            {
                return (list.PageNumber < list.TotalPages);
            }
        }
    }
}
