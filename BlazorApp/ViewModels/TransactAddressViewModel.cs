﻿using BlazorApp.Models;
using System.Collections.Generic;

namespace BlazorApp.ViewModels
{
    public class TransactAddressViewModel
    {
        public List<TransactAddress> TransactAddresses { get; set; }
        public PageViewModel PageViewModel { get; set; }
    }
}
