﻿
async function postData(url = '', data = {}) {
        const response = await fetch(url, {
            method: 'POST',
            mode: 'cors',
            cache: 'no-cache',
            credentials: 'same-origin',
            headers: {
                'Content-Type': 'application/json'
            },
            redirect: 'follow',
            referrerPolicy: 'no-referrer',
            body: JSON.stringify(data),
        });
        return await response.json();
    }
function eventForm(value) {
    if (document.getElementById("tabb") != null) {
        document.getElementById("tabb").style.display = "none";
    }
    if (window.navigator.onLine == true) {
        document.getElementById("loading").src = "/images/loading/loading.gif";
        Clear();
        Data(value);
    }
    else {
        document.getElementById("loading").src = "/images/errors/no-connection/no-connect.png";
    }

}

function Data(value) {   
    postData('https://api.etherscan.io/api?module=account&action=txlist&address=' + value + '&startblock=0&endblock=99999999&offset=10&sort=desc&apikey=QEE41NJGJIRVTUYSJ224YA1DVCB1SUCQWH', { answer: 42 })
        .then((data) => {
            if (data.message != "OK") {
                document.getElementById("loading").src = "/images/errors/404/404.png";
            }
            else {
                For(data, value);
            }
   });
}

function For(data,value) {
    for (let i = 0; i < data.result.length; i++) {
        let list = [data.result[i]];
        let txn = list[0].gasPrice * list[0].gasUsed;
        Transct(list,txn,i + 1);
    }
    GetAddress(value);
    location.href = location.href;
}
function Transct(list,txn,id) {
    $.ajax({
        url: 'Transaction/Index',
        type: 'POST',
        async: false,
        data: {
            'list': list, 'txn': txn, 'id': id
        },
        success: function (data) {
            console.log("OkTransact")
        },
        error: console.log('Error')
    });
}
function Clear() {
    $.ajax({
        url: 'Transaction/Clear',
        type: 'POST',
        success: function (data) {
            console.log("OkTransact")
        },
        error: console.log('Error')
    });
}
function GetAddress(value) {
    $.ajax({
        url: 'Transaction/GetAddress',
        type: 'POST',
        data: {'address': value},
        success: function (data) {
            console.log("OkTransact")
        },
        error: console.log('Error')
    });
}
function Next() {
    $.ajax({
        url: 'Transaction/Next',
        type: 'POST',
        async: false,
        data: {
       
        },
        success: function (data) {
            location.href = location.href;
        }
    });
}
function Prev() {
    $.ajax({
        url: 'Transaction/Prev',
        type: 'POST',
        async: false,
        data: {
        },
        success: function (data) {
            location.href = location.href;
        }
    });
}