﻿using BlazorApp.Models;
using BlazorApp.ViewModels;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;

namespace BlazorApp.Controllers
{
    public class TransactionController : Controller
    {
        [HttpGet]
        public TransactAddressViewModel Index(int page, int pageSize = 5)
        {
            List<TransactAddress> source = TransactAddress.ll;
            var count = source.Count();
            var items = source.Skip((PageViewModel.list.PageNumber - 1) * pageSize).Take(pageSize).ToList();

            PageViewModel pageViewModel = new PageViewModel(count, PageViewModel.list.PageNumber, pageSize);
            TransactAddressViewModel viewModel = new TransactAddressViewModel
            {
                PageViewModel = pageViewModel,
                TransactAddresses = items
            };
            return viewModel;
        }
        [HttpPost]
        [RequestFormLimits(ValueCountLimit = int.MaxValue)]
        public List<TransactAddress> Index(List<TransactAddress> list, long txn, long id)
        {
            if (list != null)
            {
                list[0].Id = id;
                list[0].TxnFee = txn;
                if (list[0].Input == "0x")
                {
                    list[0].Input = "Transfer";
                }
                TransactAddress.ll.AddRange(list);
            }
            return list;
        }

        [HttpGet]
        public TransactName Details()
        {
            return TransactName.nn;
        }

        [HttpPost]
        public TransactName Details(TransactName element)
        {
            if (element != null)
            {
                if (element.Status == "0x1")
                {
                    element.Status = "Success";
                }
                TransactName.nn = element;
            }
            return TransactName.nn;
        }
        [HttpPost]
        public void Clear()
        {
            PageViewModel.list = new PageViewModel();
            TransactAddress.ll = new List<TransactAddress>();
        }
        [HttpPost]
        public void Clear222()
        {
            TransactName.nn = new TransactName();
        }
        [HttpPost]
        public void GetAddress(string address)
        {
            if (address != null)
            {
                TransactAddress.Address = address;
            }
        }
        [HttpPost]
        public void Next()
        {
            PageViewModel.list.PageNumber = PageViewModel.list.PageNumber + 1;
        }
        [HttpPost]
        public void Prev()
        {
            PageViewModel.list.PageNumber = PageViewModel.list.PageNumber - 1;
        }
    }
}
