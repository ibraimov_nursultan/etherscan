﻿using System.Collections.Generic;

namespace BlazorApp.Models
{
    public class TransactAddress
    {
        public long Id { get; set; }
        public static string Address { get; set; }
        public string Hash { get; set; }
        public string Input { get; set; }
        public string BlockNumber { get; set; }
        public string To { get; set; }
        public string From { get; set; }
        public long Value { get; set; }
        public long TxnFee { get; set; }
        public static int num { get; set; }

        public static List<TransactAddress> ll = new List<TransactAddress>();
        public static int page = new int();
    }
}
