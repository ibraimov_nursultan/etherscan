﻿namespace BlazorApp.Models
{
    public class TransactName : TransactAddress
    {
        public string TransactionHash { get; set; }
        public long GasUsed { get; set; }
        public string Status { get; set; }
        public static TransactName nn = new TransactName();
    }
}
